import pyscenic
import os
import glob
import pickle
import pandas as pd
import numpy as np

def prune_mulitple_runs(regulons_list,num_runs,confidence,save_folder):
	threshold = num_runs * confidence

	#start by counting how many times regulons appear
	tf_count = {}
	for reg_list in regulons_list:
    	print(len(reg_list))
    	for regulon in reg_list:
        	tf = regulon.transcription_factor
        	if tf in tf_count.keys():
            	tf_count[tf] += 1
        	else:
        	    tf_count[tf] = 1

	valid_tfs = [tf for tf in tf_count.keys() if tf_count[tf] >= threshold]

	### within those regulons, keep only targets that are expressed 4+ times (out of 5)
	#compile dictionary that takes in tf, outputs a dictionary(key: target gene; value: count)
	tf_targets = {}
	for reg_list in regulons_list:
	    for regulon in reg_list:
	        tf = regulon.transcription_factor
	        if tf in valid_tfs:
	            if tf in tf_targets.keys(): #already have a dictionary entry
	                target_dict = tf_targets[tf]
	            else: #need to make a new entry
	                target_dict = {}
	                
	            #updating dict for each target
	            for target in regulon.gene2weight.keys():
	                if target in target_dict.keys(): #already have an entry --> just update count
	                    target_dict[target] += 1
	                else: #need to make a new entry
	                    target_dict[target] = 1
	            
	            #now that have updating target_dict, update tf_dict
	            tf_targets[tf] = target_dict

	#use information compiled above to create a dictionary - key: tf; value: final pruned list of targets
	pruned_targets = {}
	for tf in valid_tfs:
	    pruned_targets[tf] = [target for target in tf_targets[tf].keys() if tf_targets[tf][target] >= threshold]


	#compile dictionary - key: (tf,target) touple; value: list of target weights
	target_weights = {}

	for reg_list in regulons_list:
	    for regulon in reg_list:
	        tf = regulon.transcription_factor
	        if tf in valid_tfs: #take only tfs we care about
	            for target in regulon.gene2weight.keys():
	                if target in pruned_targets[tf]: #take only targets we care about
	                    #update dictionary entry
	                    if (tf,target) in target_weights.keys(): #add to existing entry
	                        target_weights[(tf,target)].append(regulon.gene2weight[target])
	                    else: #make new entry
	                        target_weights[(tf,target)] = [regulon.gene2weight[target]]

	#make regulons! using target_weights to get average weights and contstuct gene2weight for each regulon
	final_regulons = []

	for tf in valid_tfs:
	    gene2weight = {}
	    for target in pruned_targets[tf]:
	        gene2weight[target] = np.mean(target_weights[(tf,target)])
	    
	    final_regulons.append(pyscenic.genesig.Regulon(tf + '(+)',gene2weight,{},tf))

	# Save this final list of regulons!
	name = 'final_regulons_' + str(confidence) + '.p'
	fname = os.path.join(save_folder,name)
	with open(fname, "wb") as f:
	    pickle.dump(final_regulons, f)