"""
    Copyright (C) 2018  Anton Crombach (anton.crombach@inria.fr)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

DESCRIPTION

    Some functions to make the analysis of mouse (Mus musculus) piriform 
    cortex single cell/nucleus RNA sequencing data easier.
"""

import os
import collections as coll
import datetime
import itertools as itt
import textwrap as tw

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.colors as mpcol

import scanpy as sc


#
# Constants
#
NEURON = ['Bcl11a', 'Bcl11b', 'Calb1', 'Cdh8', 'Cnih3', 'Crh', 
    'Cux1', 'Dach2', 'Dlx1', 'Egr1', 'Epha7', 'Etv1', 'Fezf2', 'Fos', 'Foxg1', 
    'Foxp2', 'Gabra1', 'Gabra5', 'Gabrg2', 'Gda', 'Glra2', 'Gpr88', 'Hs3st2', 
    'Htr2c', 'Kcna1', 'Kcng1', 'Kcnip4', 'Lmo4', 'Lrrtm4', 'Mal2', 'Mef2c', 
    'Meis2', 'Neurod6', 'Nfia', 'Nfix', 'Nov', 'Npas1', 'Npas4', 'Nr2e1', 
    'Nr2f2', 'Nts', 'Pbx3', 'Pcsk2', 'Pgm2l1', 'Plcxd3', 'Pou3f2', 'Prdm8', 
    'Prox1', 'Pthlh', 'Pvalb', 'Reln', 'Rfx3', 'Rorb', 'Satb1', 'Satb2', 
    'Scg2', 'Six3', 'Sla', 'Slc12a5', 'Slc9a3r2', 'Snap25', 'Sox4', 'Sox5', 'Sox6', 
    'Sst', 'Sstr2', 'Stmn2', 'Syt1', 'Tbr1', 'Tshz1', 'Zbtb20', 'Zeb2', 
    'Zic1', 'Zic2']

PIRIFORM_NEURON = ['Btbd3', 'Cacna1s', 'Cartpt', 'Cbx6', 'Dpf1', 'Fgfr2', 
    'Foxp2', 'Id1', 'Id4', 'Kcng1', 'Lmo3', 'Lrp4', 'Mical1', 'Neurod6', 'Otx1', 
    'Prdm8', 'Prrx1', 'Pvalb', 'Reln', 'Robo4', 'Sema3a', 'Slc13a3', 'Slc22a8', 
    'Sst', 'Vamp5', 'Zic1']

# Link neurons to blood supply, clean up neurotransmitter
ASTROCYTE = ['A2m', 'Aldh1l1', 'Clcn3', 'Fst', 'Gab1', 'Gfap', 'Glis3', 
    'Itih5', 'Ogn', 'Osmr', 'Ppp1r15a', 'Rad23b', 'Rdh10', 'Rnf13', 'Rsph9',
    'S100a6', 'Tgfbr2', 'Tril']

# Wrap nerve fibres with myelin sheath
OLIGODENDROCYTE = ['Cldn11', 'Cpm', 'Enpp6', 'Fa2h', 'Gjc2', 'Gsn', 
    'Mag', 'Mal', 'Mbp', 'Mobp', 'Mog', 'Olig1', 'Sox10']
# Creation and secretion of brain fluid, beat with cilia. Perhaps stem cells
EPENDYMAL = ['Foxj1', 'Sox2', 'Vim', 'Prdx6', 'Mt1', 'Mt2', 'Slc2a1', 'Fgfr1',
    'Fgfr2']

# Macrophages that direct the immune response in the CNS
MICROGLIAL = ['Aif1', 'C1qa', 'Ctss', 'Cx3cr1', 'Gpr34', 'Hexb', 'Mrc1', 
    'P2ry12', 'Tmem119']
VASCULAR = ['Acta2', 'Cd93', 'Cldn5', 'Cxcl12', 'Tagln']
# Adult stem cells aka progenitor cells
ADULT_STEMCELL = ['Sox9']

# Stress related genes
STRESS = ['l7Rn6']

# KEGG diagram general glial cell
GLIAL = ['Abat', 'Glul', 'Slc1a2', 'Slc38a3', 'Slc38a5']

GLUTAMATERGIC = ['Adcyap1', 'Cacna1a', 'Crh', 'Gphn', 'Grm1', 'Grm2', 
    'Grm3', 'Grm4', 'Grm5', 'Kcnc1', 'Kcnc2', 'Kcnc3', 'Kcnc4', 'Kcnma1', 
    'Nrn1', 'Scn1a', 'Scn1b', 'Scn2b', 'Scn8a', 'Slc17a6', 
    'Slc17a7', 'Slc1a2', 'Spp1']
# Slc6a13 = Gat2,3 and Gatb3
GABAERGIC = ['Abat', 'Cck', 'Coch', 'Gad1', 'Gad2', 'Gadl1', 'Gls', 
    'Gls2', 'Pvalb', 'Slc6a1', 'Slc32a1', 'Slc6a13']
GLUT_GABA = ['Slc38a1', 'Slc38a2']

# Piriform-specific layer marker genes from Diodato et al. 2016
DIODATO_LAYER_1 = ['Slc13a3', 'Lrp4', 'Fgfr2', 'Zic1', 'Prrx1', 'Mical1', 
    'Vamp5', 'Slc22a8', 'Id4', 'Cacna1s', 'Robo4', 'Id1']
DIODATO_LAYER_2 = ['Slc13a3', 'Neurod6', 'Cartpt', 'Cbx6', 'Btbd3', 'Prdm8', 
    'Lmo3', 'Reln']
DIODATO_LAYER_3 = ['Dpf1', 'Sema3a', 'Neurod6', 'Sst', 'Kcng1', 'Pvalb',
    'Foxp2', 'Otx1']


# From Shibata et al. 2015
# NOTE: I've added Sox9 myself, it was not in the article. It overlaps very 
# nicely with the others!
MAINTENANCE_PROGENITOR = ['Emx1', 'Emx2', 'Foxg1', 'Hes1', 'Hes5', 'Lhx2', 
    'Pax6', 'Sox9']
# NOTE: Neurog1,2 = Ngn1,2 and both are hardly expressed.
EARLY_PROGENITOR = ['Fezf2', 'Id4', 'Nr2e1']
# NOTE: Tbr2 is not in the data, Tfap2c has been filtered out during 
# preprocessing, Insm1 is almost not expressed
LATE_PROGENITOR = ['Cux1', 'Cux2', 'Pou3f2', 'Pou3f3', 'Tbr2', 'Tfap2c', 
    'Insm1']
OLIGODENDROCYTE_PROMOTING = ['Myrf', 'Olig1', 'Olig2', 'Sox10', 'Yy1', 
    'Zfp219', 'Smarca4']
MIGRATION_PROMOTING = ['Neurog2', 'Pou3f2', 'Pou3f3', 'Rnd2', 'Sox5', 'Zfp238']
SYNAPTIC_ACTIVITY = ['Creb1', 'Arnt2', 'Npas4', 'Carf', 'Usf1', 'Crebbp', 
    'Bdnf', 'Srf']

# Neocortex layer specific genes
# NOTE: Satb2 is virtually not expressed in our data. Af9 = Mllt3
SHIBATA_LAYER_234 = ['Satb2', 'Cux1', 'Cux2', 'Bhlhe22', 'Mllt3', 'Dot1l']
SHIBATA_LAYER_4 = ['Nr2f1', 'Lhx2', 'Cux1', 'Cux2', 'Bhlhe22']
SHIBATA_LAYER_5 = ['Bcl11b', 'Fezf2', 'Sox4', 'Sox5', 'Sox11']
SHIBATA_LAYER_6 = ['Nr2f1', 'Sox5', 'Tbr1']


# Neocortex according to Saunders et al. 2017
# NOTE: Lacking on zoom: 'Grem1', 'Fibcd1', 
SAUNDERS_NEURONS = ['Rbfox3', 'Slc17a6', 'Slc17a7', 'Gad1', 
    'Pkib', 'Scn4b', 'Kcnc3', 'Elfn1', 'Grik3', 'Deptor', 'Casz1', 
    'Trdn', 'Pappa']


# Some interesting genes mentioned by Toma and Hanashima 2015
# Sip1 = Zeb2, Ring1b = Rnf2, Ctip2 = Bcl11b, Mash1 = Ascl1, Tbr2 = Eomes,
# Er81 = Etv1, Lrp8 = ApoER2, 
# Switching from deep layer to upper layer
TOMA_DL_UL = ['Ntf3', 'Zeb2', 'Rnf2', 'Fezf2', 'Bcl11b', 'Satb2']
TOMA_UL_GLIO = ['Ascl1', 'Rnf2', 'Ezh2', 'Fgf9', 'Hmga1', 'Ctf1']
TOMA_MIGRATION = ['Cxcl12', 'Cxcr4', 'Sema3a', 'Ackr3', 'Plxnd1', 'Cdk5', 'Fmr1']
TOMA_UL_INTEGRATION = ['Satb2', 'Cux2', 'Unc5d', 'Flrt3', 'Robo1', 'Epha4', 
    'Efnb1']
TOMA_DL_INTEGRATION = ['Bcl11b', 'Etv1', 'Lrp8', 'Flrt2']
TOMA_INTEGRATION = ['Nos1', 'Fmr1', 'Reln', 'Dab1']

# Hu et al. 2017
# Interneurons should also express Parvalbumin, Somatostatin, Cyclin D2 (Ccnd2), and Pten
# Nkx2.1 = Ttf1, 
INTERNEURONS = ['Dlx1', 'Dlx2', 'Dlx5', 'Dlx6', 'Lhx6', 
                'Lhx8', 'Ttf1', 'Nkx6-2', 'Npas1', 'Npas3', 
                'Olig1', 'Satb1', 'Sox6', 'Ccnd2', 'Pten',
                'Pvalb', 'Sst']

# Rubio et al. 2015
# Immature-like neurons found in piriform cortex
IMLIKE_NEURONS = ['Ncam1', 'Dcx']


#
# Functions
#
def read_piriform_adult(input_path=None):
    """Read in data, report which genes are found multiple times."""
    if input_path is None:
        print('Using built-in path')
        input_path = '../../data/PiriF_SC/filtered_gene_bc_matrices/mm10'

    data = sc.read(os.path.join(input_path, 'matrix.mtx')).T
    try:
        data.var_names = pd.read_csv(os.path.join(input_path, 'genes.tsv'), 
            header=None, sep='\t')[1]
    except FileNotFoundError:
        data.var_names = pd.read_csv(os.path.join(input_path, 'features.tsv'), 
            header=None, sep='\t')[1]
    data.obs_names = pd.read_csv(os.path.join(input_path, 'barcodes.tsv'), 
        header=None)[0]

    if sc.settings.verbosity > 2:
        # Print all genes that are present more than once as (name: found x times)
        print("\nDuplicate variable names (name: nr of occurrences):\n", 
            tw.wrap(", ".join("{0}: {1}".format(name, ndup)
                for name, ndup in itt.takewhile(lambda x: x[1] > 1, 
                    coll.Counter(data.var_names).most_common())), 70))

    # Add '-1', '-2' etc. to names to make them unique
    data.var_names_make_unique()
    return data

def calc_mito_fraction(data):
    """Calculate the fraction of mitochondrial genes per cell."""
    mito_genes = [name for name in data.var_names if name.startswith('mt-')]
    # For each cell compute fraction of counts in mito genes vs. all genes.
    # The `.A1` is only necessary because X is sparse. It transforms to a 
    # dense array after summing
    data.obs['percent_mito'] = np.sum(
        data[:, mito_genes].X, axis=1).A1 / np.sum(data.X, axis=1).A1
    # Add the total counts per cell as observations-annotation to data
    data.obs['n_counts'] = data.X.sum(axis=1).A1
    return data

def filter_by_cells_and_counts(data, min_genes=100, max_genes=5500, 
    min_cells=3, mito_frac=0.2):
    """
    Apply several filters. First, remove cells with too few genes and 
    genes with too few cells. Second, remove cells with too many genes and 
    cells with a too high fraction of mitochondrial genes."""
    sc.pp.filter_cells(data, min_genes=min_genes)
    sc.pp.filter_genes(data, min_cells=min_cells)
    
    data = calc_mito_fraction(data)
    # The actual removing of cells that express "too many" genes -- though I think
    # these are quite harmless to have in our analysis
    data = data[data.obs['n_genes'] < max_genes, :]
    # Splitting the data into with and without high fractions of mito genes
    # Currently I do not use the `mito` data, but at least I can easily if needed.
    # mito = data[data.obs['percent_mito'] > mito_frac, :]
    data = data[data.obs['percent_mito'] < mito_frac, :]
    return data

def normalize(data, counts=1e4):
    """
    Keep the log of the raw expression data, then normalize umi counts to 10k 
    per cell.
    """
    data.raw = sc.pp.log1p(data, copy=True)
    sc.pp.normalize_per_cell(data, counts_per_cell_after=counts)
    return data

def filter_by_expr_dispersion(data, n_bins, dry_run=True):
    """
    The parameters below were established through trial and error. Return log 
    expression data.
    """
    # I should check if each bin contains a normal distributed set of 
    # expression levels. Otherwise another measure of dispersion may be better.
    old_verbo = sc.settings.verbosity    
    sc.settings.verbosity = 4
    filter_result = sc.pp.filter_genes_dispersion(data.X, 
        min_mean=0.0, max_mean=100.0, min_disp=0.05, n_bins=n_bins, copy=True)
    if dry_run:
        sc.pl.filter_genes_dispersion(filter_result)
        return filter_result
    sc.settings.verbosity = old_verbo

    if not dry_run:
        # Do the actual filtering, and take the logarithm of the data
        data = data[:, filter_result.gene_subset]
        sc.pp.log1p(data)
    return data

def regress_and_scale(data):
    """
    Regress out effects of total counts per cell and the percentage of 
    mitochondrial genes expressed.
    """
    sc.pp.regress_out(data, ['n_counts', 'percent_mito'])
    # Scale the data to unit variance.
    sc.pp.scale(data)
    return data

def calc_and_plot_graph(data, pcs_used=7, neighbors=15, seed=2):
    """
    Assume that PCA has been calculated to reduce dimensions and 
    use it for spring-based projection and Louvain clustering.
    """
    sc.pp.neighbors(data, n_pcs=pcs_used, n_neighbors=neighbors)
    sc.tl.louvain(data)
    # Set nice colours
    data.uns['louvain_colors'] = [mpcol.to_hex(c) for c in sns.color_palette(
        "hls", len(data.obs['louvain'].cat.categories))]
    # Plot
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    sc.tl.draw_graph(data, random_state=4)
    sc.pl.draw_graph(data, edges=True, color='louvain', size=150, 
        legend_loc='on data', ax=ax)
    return data, fig, ax

def calc_and_plot_tsne(data, pcs_used=14, neighbors=50, seed=2):
    """
    Assume that PCA has been calculated to reduce dimensions and 
    use it for tSNE projection and Louvain clustering.
    """
    sc.pp.neighbors(data, n_pcs=pcs_used, n_neighbors=neighbors)
    sc.tl.tsne(data, random_state=seed, n_pcs=pcs_used)
    sc.tl.louvain(data)
    # Colours
    data.uns['louvain_colors'] = [mpcol.to_hex(c) for c in sns.color_palette(
        "hls", len(data.obs['louvain'].cat.categories))]
    # Plot
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    sc.pl.tsne(data, edges=True, color='louvain', legend_loc='on data', ax=ax)
    
    # sc.tl.umap(data, random_state=2)
    # sc.pl.umap(data, color='louvain', legend_loc='on data')
    return data, fig, ax

def plot_expression_on_graph(data, genenames, columns=3):
    """
    Given a list of genes, plot in n columns the graph clusters with 
    gene expression mapped onto them.
    """
    tmp = [x for x in genenames if x in data.raw.var_names]
    for i in range(0, len(tmp), columns):
        sc.pl.draw_graph(data, color=tmp[i:(i+columns)], 
            edges=True, edges_color='lightgrey', size=150)
    np = [x for x in genenames if x not in data.raw.var_names]
    if np:
        print("Not present:", np)

def plot_expression_on_graph_dark(data, genenames, columns=3):
    """
    Given a list of genes, plot in n columns the graph clusters with 
    gene expression mapped onto them.
    """
    plt.style.use('dark_background')
    tmp = [x for x in genenames if x in data.raw.var_names]
    for i in range(0, len(tmp), columns):
        sc.pl.draw_graph(data, color=tmp[i:(i+columns)], 
            edges=True, edges_color='lightgrey', size=150, color_map="cool")
    np = [x for x in genenames if x not in data.raw.var_names]
    if np:
        print("Not present:", np)
    plt.style.use('default')

def plot_expression_on_tsne(data, gene_names, columns):
    """
    Given a list of genes, plot in n columns the tSNE clusters with 
    gene expression mapped onto them.
    """
    for i in range(0, len(gene_names), columns):
        sc.pl.tsne(data, color=gene_names[i:(i+columns)])

def plot_louvain_clusters(data, cluster_names=None, suffix='_celltype.pdf'):
    """Hack cluster colours. Add names, if given, to the labels."""
    if cluster_names is not None:
        data.obs['louvain'].cat.categories = cluster_names

    if suffix is None:
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        # sc.pl.tsne(data, color='louvain', legend_loc='on data')
        sc.pl.draw_graph(data, edges=True, edges_color='lightgrey', size=150, 
            color='louvain', legend_loc='on data', ax=ax)
    else:
        # sc.pl.tsne(data, color='louvain', legend_loc='on data',
        #     save='_{}_{}'.format(timestamp(), suffix))
        sc.pl.draw_graph(data, edges=True, edges_color='lightgrey', size=150,
            color='louvain', legend_loc='on data', 
            save='_{}_{}'.format(timestamp(), suffix))
    return data

def plot_joint_expression_on_graph(data, genenames, projection):
    """
    Given a list of genes, plot together their raw expression levels.

    projection: max, min, or average

    Max will show if any of the genes is expressed in a cell.
    Min will if all of the genes are expressed in a cell.
    Avg is in between min and max.
    """
    tmp = np.array([np.array(data.raw[:, x].X)
                    for x in genenames if x in data.raw.var_names])
    # Check if any genes are missing, and report in that case
    missing = [x for x in genenames if x not in data.raw.var_names]
    if missing:
        print("Not present:", missing)

    max_name = 5
        
    if projection == 'max':
        label = 'Max of {}'.format(', '.join(genenames[:max_name]))
        data.obs[label] = np.amax(tmp, axis=0)
    elif projection == 'min':
        label = 'Min of {}'.format(', '.join(genenames[:max_name]))
        data.obs[label] = np.amin(tmp, axis=0)
    elif projection == 'avg':
        label = 'Avg of {}'.format(', '.join(genenames[:max_name]))
        data.obs[label] = np.sum(tmp, axis=0) / tmp.shape[0]

    sc.pl.draw_graph(data, color=label,
                     edges=True, edges_color='lightgrey', size=150)

def timestamp():
    return datetime.datetime.now().strftime('%Y-%m-%d_%Hh%M')

def focus_on_clusters(clustered_data, focal_name="neuro", input_path=None):
    """
    Re-read data, select only those cells that we want (i.e. neurons).
    """
    zoom = clustered_data[
        clustered_data.obs['louvain'].str.contains(focal_name), :]

    if input_path is None:
        input_path = '../../data/PiriF_SC/filtered_gene_bc_matrices/mm10'

    data = sc.read(os.path.join(input_path, 'matrix.mtx')).T
    data.var_names = pd.read_csv(os.path.join(input_path, 'genes.tsv'), 
        header=None, sep='\t')[1]
    data.obs_names = pd.read_csv(os.path.join(input_path, 'barcodes.tsv'), 
        header=None)[0]

    data.var_names_make_unique()
    data = data[zoom.obs.index]
    return data

